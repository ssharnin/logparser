﻿using LogParser;
using System;
using Xunit;

namespace XUnitTestProject1
{
    public class MetadataAndLoanLockContextParserTests
    {
        static string[] samples = new string[] {
            "[2018-11-09 14:21:11.9303-05:00] [PID:17756] [Info] [2018-11-09 14:21:11.7272-05:00] [62e357ee-bb11-4cb3-ad6a-0721c441737d] -- [LoanModifiedJob] [be940fba-2be8-42cd-a396-e5c1314ceac7] Job started for loan {57bc9566-403f-4fb9-a0b3-323429c7043b}. ",
            "[2018-11-09 14:21:11.9303-05:00] [PID:17756] [Info] [2018-11-09 14:21:11.9147-05:00] [1d990674-b116-4403-ab49-60077f098a2f] [MetadataAndLoanLockContextService] Error locking metadata for folder FX82558911-90BC-3F10-26C6-3E37AB068940 for context DocVelocity.Integration.Encompass.ContextServices.EncompassLoanContextKey. Folder FX82558911-90BC-3F10-26C6-3E37AB068940 locked by user: DV-EN_dvlis1. ",
            "[2018-11-09 14:21:11.9303-05:00] [PID:17756] [Info] [2018-11-09 14:21:11.9147-05:00] [fb919348-9626-4fd2-917c-5c2d8d9158b8] [MetadataAndLoanLockContextService] Started locking metadata for key FX51A23CFD-6FD3-86A9-1AEE-1FFDC726158D ",
            "[2018-11-09 14:21:13.0085-05:00] [PID:17756] [Info] [2018-11-09 14:21:12.3991-05:00] [fb919348-9626-4fd2-917c-5c2d8d9158b8] [MetadataAndLoanLockContextService] Locked metadata for folder FX51A23CFD-6FD3-86A9-1AEE-1FFDC726158D for context FX51A23CFD-6FD3-86A9-1AEE-1FFDC726158D " };

        [Fact]
        public void GetDateAndGuidAndComponent()
        {
            var sample = "[2018-11-09 14:21:10.5085-05:00] [PID:17756] [Info] [2018-11-09 14:21:10.5085-05:00] [85c8fe8a-8074-4857-8db0-857e46a020ba] [MetadataAndLoanLockContextService] [e7cac928-cc43-489c-85ed-6a1f633e5098] Job LoanModifiedJob was postponed for folder FXE4DCB7C9-B715-DAFE-852A-033A8B8E85B9";
            var parser = new LineParser();
            var result = parser.Parse(sample);

            Assert.Equal("85c8fe8a-8074-4857-8db0-857e46a020ba", result.CorrelationId);
            Assert.Equal(2018, result.DateTime.Year);
            Assert.Equal(11, result.DateTime.Month);
            Assert.Equal(9, result.DateTime.Day);
            Assert.Equal(14, result.DateTime.Hour);
            Assert.Equal(21, result.DateTime.Minute);
            Assert.Equal(10, result.DateTime.Second);
            Assert.Equal(508, result.DateTime.Millisecond);
            Assert.Equal("MetadataAndLoanLockContextService", result.Component);
        }

        [Fact]
        public void GetFolderIdAndStartLock()
        {
            var calls = 0;
            var parser = new LineParser();
            foreach (var sample in samples)
            {
                var result = parser.Parse(sample);
                if (result?.Action == "Started locking metadata for key")
                {
                    Assert.Equal("FX51A23CFD-6FD3-86A9-1AEE-1FFDC726158D", result.FolderId);
                    Assert.Equal("MetadataAndLoanLockContextService", result.Component);
                    calls++;
                }
            }
            Assert.Equal(1, calls);
        }

        [Fact]
        public void GetFolderIdAndErrorLock()
        {
            var calls = 0;
            var parser = new LineParser();
            foreach (var sample in samples)
            {
                var result = parser.Parse(sample);
                if (result?.Action == "Error locking metadata for folder")
                {
                    Assert.Equal("1d990674-b116-4403-ab49-60077f098a2f", result.CorrelationId);
                    Assert.Equal("FX82558911-90BC-3F10-26C6-3E37AB068940", result.FolderId);
                    Assert.Equal("MetadataAndLoanLockContextService", result.Component);
                    Assert.Equal("DV-EN_dvlis1", result.Body);
                    calls++;
                }
            }
            Assert.Equal(1, calls);
        }

        [Fact]
        public void GetFolderIdAndSuccessLock()
        {
            var calls = 0;
            var parser = new LineParser();
            foreach (var sample in samples)
            {
                var result = parser.Parse(sample);
                if (result?.Action == "Locked metadata for folder")
                {
                    Assert.Equal("FX51A23CFD-6FD3-86A9-1AEE-1FFDC726158D", result.FolderId);
                    Assert.Equal("MetadataAndLoanLockContextService", result.Component);
                    calls++;
                }
            }
            Assert.Equal(1, calls);
        }

    }
}
