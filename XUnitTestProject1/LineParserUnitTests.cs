using LogParser;
using System;
using Xunit;

namespace XUnitTestProject1
{
    public class LineParserUnitTests
    {
        static string[] samples = new string[] {
            @"[2018-11-09 14:24:36.5098-05:00] [PID:17756] [Info] [2018-11-09 14:24:36.5098-05:00] [4ceb7579-3c1c-4d3e-9a37-d85523c5eb64] [ContextualReadAnyEventState] [PLATEVT4FE51CA4-4CE1-F3CE-0181-6763EE2B9C3F] MAILITEM_FILED event occurred for context: CB9A5188A2-7DC0-B48B-D7A1-C7598399427F and search context: FXF802B382-6D78-8BAE-2F0F-FBAD8C125C50 $json$ {""""EventGenerationTime"""":""""\/Date(1541779585000)\/"""",""""EventDetail"""":{},""""Type"""":""""MAILITEM_FILED"""",""""EventQueueName"""":""""EVENT_QUEUE"""",""""SearchContextId"""":""""FXF802B382-6D78-8BAE-2F0F-FBAD8C125C50"""",""""ContextType"""":""""CB"""",""""ContextId"""":""""CB9A5188A2-7DC0-B48B-D7A1-C7598399427F"""",""""Version"""":""""1.0"""",""""EventOriginatorAccountType"""":""""APPLICATION"""",""""EventOriginatorSessionType"""":""""SITE_USER"""",""""EventOriginatorName"""":""""Integration-Framework Integration-Framework"""",""""EventOriginatorId"""":""""Integration-Framework"""",""""FriendlyName"""":""""Mailitem Filed"""",""""StringType"""":""""MAILITEM_FILED"""",""""Id"""":""""PLATEVT4FE51CA4-4CE1-F3CE-0181-6763EE2B9C3F""""}",
            @"[2018-11-09 14:27:51.0579-05:00] [PID:17756] [Info] [2018-11-09 14:27:51.0579-05:00] [21da57d8-90b4-429a-856b-6a76fe004eb6] [ContextualReadFolderEventState] [PLATEVT0ED3215E-11A2-1407-6F78-080E08CD70B1] MAILITEM_FILED event occurred for context: CB9A5188A2-7DC0-B48B-D7A1-C7598399427F and search context: FXF802B382-6D78-8BAE-2F0F-FBAD8C125C50 $json$ {""EventGenerationTime"":""\/Date(1541779615000)\/"",""EventDetail"":{},""Type"":""MAILITEM_FILED"",""EventQueueName"":""EVENT_QUEUE"",""SearchContextId"":""FXF802B382-6D78-8BAE-2F0F-FBAD8C125C50"",""ContextType"":""CB"",""ContextId"":""CB9A5188A2-7DC0-B48B-D7A1-C7598399427F"",""Version"":""1.0"",""EventOriginatorAccountType"":""APPLICATION"",""EventOriginatorSessionType"":""SITE_USER"",""EventOriginatorName"":""Integration-Framework Integration-Framework"",""EventOriginatorId"":""Integration-Framework"",""FriendlyName"":""Mailitem Filed"",""StringType"":""MAILITEM_FILED"",""Id"":""PLATEVT0ED3215E-11A2-1407-6F78-080E08CD70B1""} ",
            @"[2018-11-09 14:24:35.1660-05:00] [PID:17756] [Info] [2018-11-09 14:24:35.1660-05:00] [4ceb7579-3c1c-4d3e-9a37-d85523c5eb64] [ContextualReadFolderEventState] Event not fetched for folder FXEA6549F5-D9E0-0FE2-F001-98C3FCF52D94 ",
            @"[2018-11-09 14:24:35.4160-05:00] [PID:17756] [Info] [2018-11-09 14:24:35.4160-05:00] [4ceb7579-3c1c-4d3e-9a37-d85523c5eb64] [ContextualReadFolderPendingEventState] Event is not fetched. Pending event queue is empty ",
            @"[2018-11-09 14:24:41.4473-05:00] [PID:17756] [Info] [2018-11-09 14:24:41.4473-05:00] [4ceb7579-3c1c-4d3e-9a37-d85523c5eb64] [ContextualReadAnyEventState] [PLATEVT4FE51CA4-4CE1-F3CE-0181-6763EE2B9C3F] Moving to pending queue $json$ {""EventGenerationTime"":""\/Date(1541779585000)\/"",""EventDetail"":{},""Type"":""MAILITEM_FILED"",""EventQueueName"":""EVENT_QUEUE"",""SearchContextId"":""FXF802B382-6D78-8BAE-2F0F-FBAD8C125C50"",""ContextType"":""CB"",""ContextId"":""CB9A5188A2-7DC0-B48B-D7A1-C7598399427F"",""Version"":""1.0"",""EventOriginatorAccountType"":""APPLICATION"",""EventOriginatorSessionType"":""SITE_USER"",""EventOriginatorName"":""Integration-Framework Integration-Framework"",""EventOriginatorId"":""Integration-Framework"",""FriendlyName"":""Mailitem Filed"",""StringType"":""MAILITEM_FILED"",""Id"":""PLATEVT4FE51CA4-4CE1-F3CE-0181-6763EE2B9C3F""} ",
            @"",
            @"",
            @"",
            @"",
        };
      
        [Fact]
        public void Should_Get_ContextualReadAnyEventState_Component()
        {          
            var parser = new LineParser();
            var result = parser.Parse(samples[0]);

            Assert.Equal("4ceb7579-3c1c-4d3e-9a37-d85523c5eb64", result.CorrelationId);
            Assert.Equal(2018, result.DateTime.Year);
            Assert.Equal(11, result.DateTime.Month);
            Assert.Equal(9, result.DateTime.Day);
            Assert.Equal(14, result.DateTime.Hour);
            Assert.Equal(24, result.DateTime.Minute);
            Assert.Equal(36, result.DateTime.Second);
            Assert.Equal(509, result.DateTime.Millisecond);
            Assert.Equal("ContextualReadAnyEventState", result.Component);
            Assert.Equal("MAILITEM_FILED", result.Action);
            Assert.Equal("PLATEVT4FE51CA4-4CE1-F3CE-0181-6763EE2B9C3F", result.Body);
            Assert.Equal("FXF802B382-6D78-8BAE-2F0F-FBAD8C125C50", result.FolderId);
        }

        [Fact]
        public void Should_Get_ContextualReadFolderEventState_Component()
        {
            var parser = new LineParser();
            var result = parser.Parse(samples[1]);

            Assert.Equal("21da57d8-90b4-429a-856b-6a76fe004eb6", result.CorrelationId);
            Assert.Equal(2018, result.DateTime.Year);
            Assert.Equal(11, result.DateTime.Month);
            Assert.Equal(9, result.DateTime.Day);
            Assert.Equal(14, result.DateTime.Hour);
            Assert.Equal(27, result.DateTime.Minute);
            Assert.Equal(51, result.DateTime.Second);
            Assert.Equal(057, result.DateTime.Millisecond);
            Assert.Equal("ContextualReadFolderEventState", result.Component);
            Assert.Equal("MAILITEM_FILED", result.Action);
            Assert.Equal("PLATEVT0ED3215E-11A2-1407-6F78-080E08CD70B1", result.Body);
            Assert.Equal("FXF802B382-6D78-8BAE-2F0F-FBAD8C125C50", result.FolderId);
        }
    }
}
