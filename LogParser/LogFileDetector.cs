﻿using LogParser.Abstractions;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Threading.Tasks;

namespace LogParser
{
    public class LogFileDetector : ILogFileDetector
    {
        private readonly IConfiguration config;
        private readonly ILogger<LogFileDetector> logger;

        public LogFileDetector(IConfiguration config, ILogger<LogFileDetector> logger)
        {
            this.config = config;
            this.logger = logger;
        }
        public string[] Detect()
        {
            return Directory.GetFiles(config["folder:path"]);
        }
    }
}
