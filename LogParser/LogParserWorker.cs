﻿using LogParser.Abstractions;
using Microsoft.Extensions.Logging;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace LogParser
{
    public class LogParserWorker : ILogParserWorker
    {
        private const int DefaultBufferSize = 4096;
        private const FileOptions DefaultOptions = FileOptions.Asynchronous | FileOptions.SequentialScan;

        private readonly IMySQLStore mySQLStore;
        private readonly ILineParser lineParser;
        private readonly ILogger<LogParserWorker> logger;

        public LogParserWorker(IMySQLStore mySQLStore, ILineParser lineParser, ILogger<LogParserWorker> logger)
        {
            this.mySQLStore = mySQLStore;
            this.lineParser = lineParser;
            this.logger = logger;            
        }
        public async Task ParseAsync(string[] filepathes, CancellationToken cancellationToken)
        {
            var tasks = new List<Task>();
            foreach (var filepath in filepathes)
            {
                tasks.Add(ParseFileAsync(filepath, cancellationToken));
            }
            await Task.WhenAll(tasks.ToArray());
        }

        private async Task ParseFileAsync(string path, CancellationToken cancellationToken)
        {
            var tasks = new List<Task>();
            var file = new FileInfo(path);
            logger.LogInformation($"Start ParseFile {file.Name} located {file.DirectoryName}");

            int linesNumber = File.ReadLines(path).Count();
            logger.LogInformation($"File {file.Name} has {linesNumber} lines.");
            // Open the FileStream with the same FileMode, FileAccess
            // and FileShare as a call to File.OpenText would've done.
            using (var stream = file.Open(FileMode.Open, FileAccess.Read, FileShare.Read))
            using (var reader = new StreamReader(stream))
            {
                int linenumber = 0;
                string line;
                while (!cancellationToken.IsCancellationRequested && (line = await reader.ReadLineAsync().ConfigureAwait(false)) != null)
                {
                    linenumber++;
                    var parseTask = new ParseTask {
                        Line = line,
                        CancellationToken = cancellationToken,
                        LineNumber = linenumber,
                        FileName = file.Name,
                        LinesCount = linesNumber
                    };

                    await ParseLine(parseTask).ConfigureAwait(false);
                }
                logger.LogInformation($"Finish ParseFile {file.Name}. Parsed {linenumber} lines from {linesNumber}.");
            }      
        }

        private async Task ParseLine(ParseTask parseTask)
        {
            var parseResult = lineParser.Parse(parseTask.Line);
            if (parseResult != null && !parseTask.CancellationToken.IsCancellationRequested)
            {
                parseResult.FileName = parseTask.FileName;
                parseResult.LineNumber = parseTask.LineNumber;                
                await mySQLStore.SaveAsync(parseResult).ConfigureAwait(false);
            }
            if (parseTask.LineNumber % 10000 == 0)
                logger.LogInformation($"Parsing File {parseTask.FileName}. Has parsed {parseTask.LineNumber} lines from {parseTask.LinesCount} for now.");
        }

        internal class ParseTask {
            public int LinesCount { get; set; }
            public int LineNumber { get; set; }
            public string FileName { get; set; }
            public string Line { get; set; }
            public CancellationToken CancellationToken { get; set; }
        }
    }
}
