﻿using LogParser.Abstractions;
using LogParser.MySQL;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using NLog.Extensions.Logging;
using System;
using System.IO;

namespace LogParser
{
    class Program
    {

        static void Main(string[] args)
        {
            var hostBuilder = new HostBuilder()
                .ConfigureAppConfiguration(ConfigureAppConfiguration)
                .ConfigureServices(ConfigurateServices);
            try
            {
                hostBuilder.Build().Run();
            }
            catch (Exception)
            {
                throw;
            }

        }

        private static void ConfigureAppConfiguration(HostBuilderContext hostBuilderContext, IConfigurationBuilder configurationBuilder)
        {
            configurationBuilder.SetBasePath(Directory.GetCurrentDirectory() + @"\Configs\");
            configurationBuilder.AddJsonFile("jsconfig.json", optional: false, reloadOnChange: true);
        }

        private static void ConfigurateServices(HostBuilderContext hostBuilderContext, IServiceCollection services)
        {
            services.AddSingleton<ILoggerFactory, LoggerFactory>();
            services.AddSingleton(typeof(ILogger<>), typeof(Logger<>));
            services.AddLogging(ConfigureLoggingBuilder);
            //configure NLog
            ConfigureNlog(services);

            services.AddTransient<ILogFileDetector, LogFileDetector>();
            services.AddScoped<IMySQLStore, MySQLStore>();
            services.AddTransient<ILineParser, LineParser>();
            services.AddTransient<ILogParserWorker, LogParserWorker>();
            services.AddScoped<IHostedService, LogParseService>();
        }

        private static void ConfigureLoggingBuilder(ILoggingBuilder loggingBuilder)
        {
            loggingBuilder.AddNLog();
            loggingBuilder.SetMinimumLevel(LogLevel.Trace);
        }

        private static void ConfigureNlog(IServiceCollection services)
        {
            var serviceProvider = services.BuildServiceProvider();
            var loggerFactory = serviceProvider.GetRequiredService<ILoggerFactory>();

            loggerFactory.AddNLog(new NLogProviderOptions { CaptureMessageTemplates = true, CaptureMessageProperties = true });

            var config = new NLog.Config.LoggingConfiguration();

            var logfile = new NLog.Targets.FileTarget("logfile") { FileName = @"logs\log.txt" };
            var tracefile = new NLog.Targets.FileTarget("tracefile") { FileName = @"logs\trace.txt" };
            var logconsole = new NLog.Targets.ConsoleTarget("logconsole");

            config.AddRule(NLog.LogLevel.Debug, NLog.LogLevel.Fatal, logconsole);
            config.AddRule(NLog.LogLevel.Debug, NLog.LogLevel.Fatal, logfile);
            config.AddRule(NLog.LogLevel.Trace, NLog.LogLevel.Fatal, tracefile);

            NLog.LogManager.Configuration = config;

        }
    }
}
