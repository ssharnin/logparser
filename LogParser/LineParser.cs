﻿using LogParser.Abstractions;
using LogParser.ComponentParsers;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Text;
using System.Text.RegularExpressions;

namespace LogParser
{
    public class LineParser : ILineParser
    {
        private List<string> components;

        private string regexDateAndGuidAndType = @"[]][ ][[](?<date>\d{4}-\d{2}-\d{2}\s\d{2}:\d{2}:\d{2}.\d{4}-\d{2}:\d{2})+[]][ ][[](?<guid>[0-9a-f]{8}[-]+(?:[0-9a-f]{4}[-]){3}[0-9a-f]{12})+[]][ ][[](?<component>[0-9a-zA-Z]+)";

        public LineParser()
        {
            components = new List<string> {
                "MetadataAndLoanLockContextService",
            //    "ContextualReadAnyEventState",
            //    "ContextualReadFolderEventState"
            };
        }

        public ParseResult Parse(string line)
        {
            var result = new ParseResult();
            var dateAndGuidAndType = Regex.Match(line, regexDateAndGuidAndType);
            if (dateAndGuidAndType.Success)
            {
                result.DateTime = DateTimeOffset.ParseExact(dateAndGuidAndType.Groups[1].Value, "yyyy-MM-dd HH:mm:ss.ffffzzz", CultureInfo.InvariantCulture);
                result.CorrelationId = dateAndGuidAndType.Groups[2].Value;
                result.Component = dateAndGuidAndType.Groups[3].Value;
            }
            else
             return null;

            if (components.Contains(result.Component))
            {
                return ParseByComponent(line, result);
            }
            else
                return null;
        }

        private ParseResult ParseByComponent(string line, ParseResult result)
        {
            switch (result.Component)
            {
                case "MetadataLockContextService":
                    return  new MetadataLockContextParser().Execute(line, result);

                case "MetadataAndLoanLockContextService":
                    return new MetadataAndLoanLockContextParser().Execute(line, result);

                case "ContextualReadAnyEventState":
                    return new ContextualReadAnyEventStateParser().Execute(line, result);

                case "ContextualReadAnyPendingEventState":
                    return new ContextualReadAnyEventStateParser().Execute(line, result);

                case "ContextualReadFolderEventState":
                    return new ContextualReadAnyEventStateParser().Execute(line, result);

                case "ContextualReadFolderPendingEventState":
                    return new ContextualReadAnyEventStateParser().Execute(line, result);

                default:
                    return result;
            }            
        }
    }
}
