﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Text.RegularExpressions;

namespace LogParser.ComponentParsers
{
    public class MetadataLockContextParser 
    {
        private string regexMetadataAction = @"[M][e][t][a][A-Za-z \]]{20}[c][e][]]+.(?<action>[A-Z][a-z ]+)";
        private string regexStartLock = @"(?<folderId>[F][X][0-9A-F]{8}[-]+(?:[0-9A-F]{4}[-]){3}[0-9A-F]{12})";
        private string regexErrorLock = @"(?<folderId>[F][X][0-9A-F]{8}[-]+(?:[0-9A-F]{4}[-]){3}[0-9A-F]{12})+(?<beforeuser>[0-9A-Za-z \.\-]{144}[u][s][e][r][:].)+(?<user>.+)+[.]";
        private string regexSuccessLock = @"(?<folderId>[F][X][0-9A-F]{8}[-]+(?:[0-9A-F]{4}[-]){3}[0-9A-F]{12})+[A-Za-z ]{13}(?<folderId2>[F][X][0-9A-F]{8}[-]+(?:[0-9A-F]{4}[-]){3}[0-9A-F]{12})";

        public ParseResult Execute(string line, ParseResult result)
        {
            var startLock = Regex.Match(line, regexMetadataAction + regexStartLock);
            if (startLock.Success)
            {
                result.Action = startLock.Groups[1].Value.TrimEnd();
                result.FolderId = startLock.Groups[2].Value;
            }

            var errorLock = Regex.Match(line, regexMetadataAction + regexErrorLock);
            if (errorLock.Success)
            {
                result.Action = startLock.Groups[1].Value.TrimEnd();
                result.FolderId = errorLock.Groups[2].Value;
                result.Body = errorLock.Groups[4].Value;
            }

            var successLock = Regex.Match(line, regexMetadataAction + regexSuccessLock);
            if (successLock.Success)
            {
                result.Action = startLock.Groups[1].Value.TrimEnd();
                result.FolderId = successLock.Groups[2].Value;
            }

            return result;
        }
    }
}
