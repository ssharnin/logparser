﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Text.RegularExpressions;

namespace LogParser.ComponentParsers
{
    public class ContextualReadAnyEventStateParser 
    {
        private string regexPlatformEventId = @"(?<platformEventId>[P][L][A][T][E][V][T][0-9A-F]{8}[-]+(?:[0-9A-F]{4}[-]){3}[0-9A-F]{12})+[]][ ]+";
        private string regexEventType = @"(?<eventType>[A-Z_]+)[0-9A-Za-z: -]+";
        private string regexFolderId = @"(?<folderId>[F][X][0-9A-F]{8}[-]+(?:[0-9A-F]{4}[-]){3}[0-9A-F]{12})";
         
        public ParseResult Execute(string line, ParseResult result)
        {
            var contextualReadAnyEvent = Regex.Match(line,  regexPlatformEventId + regexEventType + regexFolderId);
            if (contextualReadAnyEvent.Success)
            {
                result.Body = contextualReadAnyEvent.Groups[1].Value;
                result.Action = contextualReadAnyEvent.Groups[2].Value;
                result.FolderId = contextualReadAnyEvent.Groups[3].Value;
            }

            return result;
        }
    }
}
