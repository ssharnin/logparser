﻿using LogParser;
using LogParser.Abstractions;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace LogParser.MySQL
{
    public class MySQLStore : IMySQLStore
    {
        private readonly ILogger<MySQLStore> logger;

        // "server=127.0.0.1;port=3306;database=integrationdb;uid=root;password=123456aA!"
        private readonly string _connectionString;

        private const string CREATE_TABLE_PARSE_RESULTS_SQL = "CREATE TABLE IF NOT EXISTS  ParseResults (" +
            "id MEDIUMINT NOT NULL AUTO_INCREMENT," +
            "CorrelationId varchar(50) NOT NULL, " +
            "FolderId varchar(50)," +
            "Component varchar(100)," +
            "Action varchar(200)," +
            "Body varchar(1000)," +
            "Duration MEDIUMINT," +
            "DateTime Timestamp," +
            "DateTimeUTC Timestamp," +
            "LineNumber MEDIUMINT," +
            "FileName varchar(100)," +
            "PRIMARY KEY(id));";
        private const string INSERT_PARSE_RESULTS_SQL = "INSERT INTO ParseResults" +
            "(CorrelationId," +
            "FolderId," +
            "Component," +
            "Action," +
            "Body," +
            "Duration," +
            "DateTime," +
            "DateTimeUTC," +
            "LineNumber," +
            "FileName)" +
            " VALUES(" +
            "@CorrelationId," +
            "@FolderId, " +
            "@Component, " +
            "@Action, " +
            "@Body, " +
            "@Duration, " +
            "@DateTime, " +
            "@DateTimeUTC, " +
            "@LineNumber, " +
            "@FileName)";

        public MySQLStore(IConfiguration config, ILogger<MySQLStore> logger)
        {
            this.logger = logger;
            _connectionString = config["mysql:connection"]; ;
            Init();
        }

        private void Init()
        {
            try
            {
                logger.LogTrace("Connecting to " + _connectionString);
                using (var connection = new MySqlConnection(_connectionString))
                {
                    connection.Open();
                    using (var command = new MySqlCommand(CREATE_TABLE_PARSE_RESULTS_SQL, connection))
                    {
                        command.ExecuteNonQuery();
                        logger.LogInformation("ParseResults table was created");
                    }
                }
            }
            catch (Exception e)
            {
                logger.LogError("MySQl Init failed", e, e.Message);
                logger.LogInformation("ERROR " + e.Message);
                throw;
            }
        }

        public async Task SaveAsync(ParseResult parseResult)
        {
            try
            {

                using (var connection = new MySqlConnection(_connectionString))
                {
                    await connection.OpenAsync().ConfigureAwait(false);

                    using (var cmd = new MySqlCommand(INSERT_PARSE_RESULTS_SQL, connection))
                    {
                        InitEventParameters(cmd, parseResult);
                        await cmd.ExecuteNonQueryAsync().ConfigureAwait(false);
                        //logger.LogTrace($"Loan with Guid = {parseResult.CorrelationId} with FolderId = {parseResult.FolderId} saved into DB");

                    }
                }
            }
            catch (Exception e)
            {
                logger.LogError("MySQl Save failed", e, e.Message);
                logger.LogTrace(e.Message);
                throw;
            }
        }
        
        private static void InitEventParameters(MySqlCommand cmd, ParseResult parseResult)
        {
            cmd.Parameters.AddWithValue("@CorrelationId", parseResult.CorrelationId);
            cmd.Parameters.AddWithValue("@FolderId", parseResult.FolderId);
            cmd.Parameters.AddWithValue("@Component", parseResult.Component);
            cmd.Parameters.AddWithValue("@Action", parseResult.Action);
            cmd.Parameters.AddWithValue("@Body", parseResult.Body);
            cmd.Parameters.AddWithValue("@Duration", parseResult.Duration.Milliseconds);
            cmd.Parameters.AddWithValue("@DateTime", parseResult.DateTime.DateTime);
            cmd.Parameters.AddWithValue("@DateTimeUTC", parseResult.DateTime.ToUniversalTime().DateTime);
            cmd.Parameters.AddWithValue("@LineNumber", parseResult.LineNumber);
            cmd.Parameters.AddWithValue("@FileName", parseResult.FileName);
        }

    }
}
