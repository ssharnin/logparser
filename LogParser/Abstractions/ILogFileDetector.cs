﻿using System.Threading.Tasks;

namespace LogParser.Abstractions
{
    public interface ILogFileDetector
    {
        string[] Detect();
    }
}