﻿using System.Threading.Tasks;

namespace LogParser.Abstractions
{
    public interface IMySQLStore
    {
        Task SaveAsync(ParseResult parseResult);
    }
}