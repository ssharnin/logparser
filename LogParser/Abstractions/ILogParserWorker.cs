﻿using System.IO;
using System.Threading;
using System.Threading.Tasks;

namespace LogParser.Abstractions
{
    public interface ILogParserWorker
    {
        Task ParseAsync(string[] filepathes, CancellationToken cancellationToken);
    }
}