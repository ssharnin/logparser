﻿namespace LogParser.Abstractions
{
    public interface ILineParser
    {
        ParseResult Parse(string line);
    }
}