﻿using LogParser.Abstractions;
using Microsoft.Extensions.Hosting;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace LogParser
{
    public class LogParseService : IHostedService
    {
        private readonly ILogParserWorker logParserWorker;
        private readonly ILogFileDetector logsFileDetector;

        public LogParseService(ILogParserWorker logParserWorker, ILogFileDetector logsFileDetector)
        {
            this.logParserWorker = logParserWorker;
            this.logsFileDetector = logsFileDetector;
        }

        public async Task StartAsync(CancellationToken cancellationToken)
        {
            await logParserWorker.ParseAsync(logsFileDetector.Detect(), cancellationToken).ConfigureAwait(false);
        }

        public async Task StopAsync(CancellationToken cancellationToken)
        {
            await Task.Run(() => NLog.LogManager.Shutdown()).ConfigureAwait(false);
        }
    }
}
