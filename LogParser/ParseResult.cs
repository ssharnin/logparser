﻿using System;

namespace LogParser
{
    public class ParseResult
    {
        public string CorrelationId { get; set; }
        public string FolderId { get; set; }
        public string Body { get; set; }
        public string Component { get; set; }
        public string Action { get; set; }
        public TimeSpan Duration { get; set; }
        public DateTimeOffset DateTime { get; set; }
        public int LineNumber { get; set; }
        public string FileName { get; set; }
    }
}